# Windows Checksum

Simple script to run `certutil -hashfile “{external}/{filename}.zip MD5` from an external drive


## Setup

Create python virtual environment and install packages specified in `requirements.txt`

## Run

`python run_checksum.py`
