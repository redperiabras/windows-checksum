#!/usr/bin/env python
# coding: utf-8

# # Windows File Checksum

# In[45]:


import os
import re
import glob
import subprocess

import pandas as pd
from datetime import datetime


# In[46]:


drive = 'F:/'


# ## Read Directory

# In[47]:


os.chdir(drive)
migration_key = glob.glob('*.zip')


# ## Compare checksums

# In[48]:


files = []

for item in migration_key:
    item, ext = item.split('.')
    
    try:
        with open(f'./{item}_migration.Log', 'r') as f:
            data = f.read().split('\n')
            data = list(filter(lambda x: re.match(".*File checksum in*", x), data))
            data = [item.split(' ') for item in data]
            edge_checksum = data[0][12]
            external_checksum = data[1][12]
    except:
        edge_checksum = ""
        external_checksum = ""
        
    response = subprocess.run(["powershell", "-Command", f'certutil -hashfile ./{item}.zip MD5'], capture_output=True)
    response = response.stdout.decode('utf-8')
    response = response.split('\n')
    windows_checksum = response[1].strip()
    
    files.append((item, edge_checksum, external_checksum, windows_checksum))


# In[56]:


summary = pd.DataFrame(files, columns=['file', 'edge_checksum', 'external_checksum', 'windows_checksum'])
summary['is_equal'] = (summary['edge_checksum'] == summary['external_checksum']) == (summary['external_checksum'] == summary['windows_checksum'])


# In[57]:


summary


# In[58]:


summary.to_csv(f'./Export Summary - {datetime.now().date()}.csv', encoding='utf-8', index=False)


# In[59]:


equal_count, _ = summary[summary.is_equal == True].shape
total_count, _ = summary.shape


# In[60]:


print(f"{equal_count}/{total_count} zip files matched.")


# In[ ]:




